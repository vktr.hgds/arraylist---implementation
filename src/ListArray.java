import java.util.Arrays;

public class ListArray<T extends Object> implements ListArrayMethods<T> {

	T[] list;
	private int size = 10;
	private final int DEFAULT_SIZE = 10;
	private int counter = 0;
	
	
	
	//constructor
	@SuppressWarnings("unchecked")
	public ListArray() {
		this.list = (T[]) new Object[DEFAULT_SIZE];
	}
	
	
	
	//add element
	public void add(T element) {
		if (this.counter % size == 0 && this.counter >= DEFAULT_SIZE) {
			list = createNewArray();
		}
		
		list[counter] = element;
		counter++;
	}
	
	
	
	//create new array
	@SuppressWarnings("unchecked")
	private T[] createNewArray() {	
		T[] array = (T[]) new Object[size * 2];
		size = size * 2;
		
		for (int i = 0; i < list.length; i++) {
			array[i] = list[i];
		}
		
		
		
		list = array;
		return list;
		
		
		//other option
		/*
		T[] copyArray = Arrays.copyOf(list, size * 2);
		size = size * 2;
		list = copyArray;
		*/
		
	}
	
	
	
	//remove element by name
	@Override
	public void remove(T element) {
		
		if (element == null) return;
		
		if (this.counter <= 0) {
			System.out.println("List is empty, cannot delete element.");
			return;
		}
		
		boolean found = false;
		int j = 0;
		
		for (int i = 0; i < counter; i++) {
			
			if ((list[i].equals(element) || found) && i < (counter - 1)) {
				j = i;
				list[j] = list[j + 1];
				j++;
				found = true;
				
			} else if (list[i].equals(element) && i == (counter - 1)) {
				found = true;
			}
		}
		
		if (found) {
			list[counter - 1] = null;
			counter--;
		}
		
		System.out.println(found ? "Removed " + element : "Couldn't find: " + element);
	}
	
	
	
	//remove element by index
	@Override
	public void remove(int index) {
		
		T element = list[index];
		int startIndex = index;
		boolean found = false;
		
		if (index > counter || index < 0) {
			System.out.println("Error, invalid index");
			return;
		}
		
		if (index == counter - 1) {
			found = true;
		}
		
		for (int i = 0; i < counter - 1; i++) {
			if (index <= i) {
				list[index] = list[i + 1];
				index++;
				found = true;
			}
		}
		
		
		if (found) {
			list[counter - 1] = null;
			counter--;
		}
		
		System.out.println("Removed element " + element + " on index " + startIndex);
	}
	
	
	
	//print list
	@Override
	public void printList() {
		if (this.counter <= 0) {
			System.out.println("List is empty.");
			return;
		}
		
		int i = 0;
		for (T element : list) {
			i++;
			if (element != null) {
				System.out.print(i <= (this.counter - 1) ? element + " - " : element );
			}
		}
		System.out.println();
	}
	
	
	
	//get element by index
	@Override
	public void get (int i) {
		if (i > this.counter || i < 0) {
			System.out.println("Couldn't find element on index " + i);
			return;
		}
		System.out.println("Element value: " + list[i]);
	}
	
	
	
	//get element by name
	@Override
	public void get (T element) {
		
		if (element == null) return;
		
		boolean found = false;
		int i = 0;
		int occurrences = 0;
		
		for (T existingElement : list) {
			if (element.equals(existingElement) && i < counter) {
				occurrences++;
				found = true;
				System.out.println("Element " + "(" + element + ")" + " found on index " + i);
			} 
			i++;
		}
		
		System.out.println(occurrences > 0 ? "All occurences: " + occurrences : "");
		
		if (!found) {
			System.out.println("Couldn't find element " + element);
		}
	}
	
	
	
	//get the number of elements in the list (null values doesn't count)
	@Override
	public void elementCount() {
		System.out.println("Elements in the list: " + this.counter);
	}
	
	
	
	//get the size/length of the list - null values count too
	@Override
	public void listSize() {
		System.out.println("Size of the list: " + this.size);
	}
	
	
	
	//clear list
	@Override
	@SuppressWarnings("unused")
	public void clear() {
		
		if (this.counter <= 0) {
			System.out.println("List is already empty");
			return;
		}
		
		for (T element : list) {
			element = null;
		}
		counter = 0;
		size = 10;	
	}

}
