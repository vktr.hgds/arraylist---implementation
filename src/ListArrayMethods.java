
public interface ListArrayMethods<T> {
	//add element
	public void add(T element);
	
	//remove element by name
	public void remove(T element);
	
	//remove element by index
	public void remove(int index);
	
	//get element by name
	public void get (T element);
	
	//get element by index
	public void get (int index);
	
	//print ListArray to the console
	public void printList();
	
	//get the number of elements
	public void elementCount();
	
	//get the length of the list (null values are included)
	public void listSize();
	
	//make the list empty
	public void clear();
	
}
