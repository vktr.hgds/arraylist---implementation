import java.util.Random;

public class Main {
	
	public static void main(String[] args) {
		
		ListArray<Integer> list = new ListArray<Integer>();
		Random rand = new Random();
		
		for (int i = 0; i < 19; i++) {
			int r = rand.nextInt(20);
			list.add(r);
		}
		list.add(100);
		
		list.printList();
		list.elementCount();
		list.listSize();
		
		//remove object
		list.remove(new Integer(100));
		
		System.out.println();
		list.printList();
		list.elementCount();
		list.listSize();
		
		//remove element on index 10
		list.remove(10);
		
		System.out.println();
		list.printList();
		list.elementCount();
		list.listSize();
		
		System.out.println();
		list.get(1);
		list.get(100);
		list.get(new Integer(1));
		
		System.out.println();
		list.clear();
		list.printList();
		list.elementCount();
		list.listSize();
		
		System.out.println();
		list.clear();
	}
}
